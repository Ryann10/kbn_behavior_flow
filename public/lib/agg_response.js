import _ from 'lodash';
import arrayToLinkedList from 'ui/agg_response/hierarchical/_array_to_linked_list';

module.exports = function sankeyProvider(Private, Notifier) {

  const notify = new Notifier({
    location: 'Sankey chart response converter'
  });

  let nodes = {};
  let links = {};
  let lastNode = -1;
  let maxLevel = 0;

  function processEntry(aggConfig, metric, aggData) {
    _.each(aggData.buckets, function(b) {
      if (aggConfig._next) {
        processChildEntry(aggConfig._next, metric, b[aggConfig._next.id]);
      }
    });
  }

  function processChildEntry(aggConfig, metric, aggData) {
    let prevBuckectNode = -1;
    let level = 0;
    let index = '';
    let normarlized = [];

    _.each(aggData.buckets, function(b) {
      if (b[aggConfig._next.id].buckets.length > 1) {
        for (let i = 0; i < b[aggConfig._next.id].buckets.length; i++) {
          const copy = _.clone(b, true);
          const bucket = b[aggConfig._next.id].buckets[i];

          copy[aggConfig._next.id].buckets = [bucket];

          normarlized.push(copy);
        }
      } else {
        normarlized.push(b);
      }
    });

    normarlized.sort(function(a, b) {
      return a[aggConfig._next.id].buckets[0].key - b[aggConfig._next.id].buckets[0].key;
    });

    if (normarlized[0].key !== 'CHECK_TEST') {
      return;
    }

    _.each(normarlized, function(b) {
      index = level + b.key;
      level += 1;

      if (isNaN(nodes[index])) {
        nodes[index] = lastNode + 1;
        lastNode = _.max(_.values(nodes));
      }

      if (aggConfig._previous) {
        var k;
        if (prevBuckectNode === -1) {
          prevBuckectNode = nodes[index];
        } else {
          k = prevBuckectNode + 'sankeysplitchar' + nodes[index];
          prevBuckectNode = nodes[index];

          if (isNaN(links[k])) {
            links[k] = 1;
          } else {
            links[k] += 1;
          }
        }
      }
    });

    if (maxLevel < level) {
      maxLevel = level;
    }
  }

  return function(vis, resp) {

    let metric = vis.aggs.bySchemaGroup.metrics[0];
    let buckets = vis.aggs.bySchemaGroup.buckets;
    buckets = arrayToLinkedList(buckets);
    if (!buckets) {
      return {
        'slices': {
          'nodes': [],
          'links': []
        }
      };
    }

    let firstAgg = buckets[0];
    let aggData = resp.aggregations[firstAgg.id];

    if (!firstAgg._next) {
      notify.error('need more than one sub aggs');
    }

    nodes = {};
    links = {};
    lastNode = -1;

    processEntry(firstAgg, metric, aggData);

    let invertNodes = _.invert(nodes);

    let chart = {
      'slices': {
        'nodes': _.map(_.keys(invertNodes), function(k) {
          return {
            'name': invertNodes[k]
          };
        }),
        'links': _.map(_.keys(links), function(k) {
          let s = k.split('sankeysplitchar');
          return {
            'source': parseInt(s[0]),
            'target': parseInt(s[1]),
            'value': links[k]
          };
        }),
        'depth': maxLevel
      }
    };

    return chart;
  };
};
