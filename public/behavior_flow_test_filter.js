import 'ui/agg_table';
import 'ui/agg_table/agg_table_group';

import 'plugins/behavior_flow_test_filter/behavior_flow_test_filter.less';
import 'plugins/behavior_flow_test_filter/behavior_flow_test_filter';

import TemplateVisTypeTemplateVisTypeProvider from 'ui/template_vis_type/template_vis_type';
import VisSchemasProvider from 'ui/vis/schemas';
import behaviorFlowVisTemplate from 'plugins/behavior_flow_test_filter/behavior_flow_test_filter.html';

require('ui/registry/vis_types').register(BehaviorFlowVisProvider);

function BehaviorFlowVisProvider(Private) {
  const TemplateVisType = Private(TemplateVisTypeTemplateVisTypeProvider);
  const Schemas = Private(VisSchemasProvider);

  return new TemplateVisType({
    name: 'behavior_flow_test_filter',
    title: 'Behavior Flow(test filter)',
    icon: 'fa-random',
    description: 'Behavior flow is ideal for displaying the event flows.',
    template: behaviorFlowVisTemplate,
    params: {
      defaults: {
        showMetricsAtAllLevels: false
      },
      editor: '<vislib-basic-options></vislib-basic-options>'
    },
    hierarchicalData: function (vis) {
      return Boolean(vis.params.showPartialRows || vis.params.showMetricsAtAllLevels);
    },
    schemas: new Schemas([{
      group: 'metrics',
      name: 'metric',
      title: 'Split Size',
      min: 1,
      max: 1,
      defaults: [{
        type: 'count',
        schema: 'metric'
      }]
    }, {
      group: 'buckets',
      name: 'segment',
      title: 'Split Slices',
      aggFilter: '!geohash_grid',
      min: 0,
      max: Infinity
    }]),
    requiresSearch: true
  });
}
