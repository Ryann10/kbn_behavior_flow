export default function (kibana) {
  return new kibana.Plugin({
    uiExports: {
      visTypes: [
        'plugins/behavior_flow_test_filter/behavior_flow_test_filter'
      ]
    }
  });
}
