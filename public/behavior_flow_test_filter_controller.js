import uiModules from 'ui/modules';

const module = uiModules.get('kibana/behavior_flow_test_filter', ['kibana']);

import d3 from 'd3';
import _ from 'lodash';
import $ from 'jquery';
import S from 'd3-plugins-sankey-fixed';

import AggResponseProvider from './lib/agg_response';

module.controller('BehaviorFlowController', function($scope, $element, $rootScope, Private) {
  const sankeyAggResponse = Private(AggResponseProvider);

  let svgRoot = $element[0];
  let color = d3.scale.category10();
  let margin = 20;
  let width;
  let height;
  let div;
  let svg;
  let globalData = null;
  let depth;
  const defaultColumnWidth = 250;

  let _updateDimensions = function _updateDimensions(depth) {
    let delta = 10;
    let w = $(svgRoot).parent().width() - 10;
    let h = $(svgRoot).parent().height() - 40;
    if (w) {
      if (w > delta) {
        w -= delta;
      }
      width = w < depth * defaultColumnWidth ? depth * defaultColumnWidth : w;
    }
    if (h) {
      if (h > delta) {
        h -= delta;
      }
      height = h;
    }
    $(svgRoot).width(width);
  };

  let _removeLevelFromName = function _removeLevelFromName(name) {
    if (name.substring(1, 4) == "1:1" || isNaN(name[1])) {
      return name.substring(1);
    }
    return name.substring(2);
  };

  let _buildVis = function(data) {
    let energy = data.slices;

    _updateDimensions(energy.depth);

    d3.select(svgRoot).selectAll('svg').remove();

    div = d3.select(svgRoot);
    if (!energy.nodes.length) return;

    svg = div.append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', 'translate(0, 0)');

    $scope.$on('change:vis', function () {
      _updateDimensions(energy.depth);
      _buildVis(globalData);
    });

    let sankey = d3.sankey()
      .nodeWidth(15)
      .nodePadding(10)
      .size([width, height]);

    let path = sankey.link();

    sankey
      .nodes(energy.nodes)
      .links(energy.links)
      .layout(32);

    let link = svg.append('g').selectAll('.link')
      .data(energy.links)
      .enter().append('path')
      .attr('class', 'link')
      .attr('d', path)
      .style('stroke-width', function(d) {
        return Math.max(1, d.dy);
      })
      .sort(function(a, b) {
        return b.dy - a.dy;
      });

    link.append('title')
      .text(function(d) {
        let percentage = ((d.value / d.source.value) * 100).toFixed(2);
        return _removeLevelFromName(d.source.name) + ' → ' + _removeLevelFromName(d.target.name) + '\n' + d.value + '(' + percentage + '%)';
      });

    let node = svg.append('g').selectAll('.node')
      .data(energy.nodes)
      .enter().append('g')
      .attr('class', 'node')
      .attr('transform', function(d) {
        return 'translate(' + d.x + ',' + d.y + ')';
      });

    node.append('rect')
      .attr('height', function(d) {
        return d.dy;
      })
      .attr('width', sankey.nodeWidth())
      .style('fill', function(d) {
        d.color = color(d.name);
        return d.color;
      })
      .style('stroke', function(d) {
        return d3.rgb(d.color).darker(2);
      })
      .append('title')
      .text(function(d) {
        return _removeLevelFromName(d.name) + '\n' + d.value;
      });

    node.append('text')
      .attr('x', -6)
      .attr('y', function(d) {
        return d.dy / 2;
      })
      .attr('dy', '.35em')
      .attr('text-anchor', 'end')
      .attr('transform', null)
      .text(function(d) {
        return _removeLevelFromName(d.name) + '[' + d.value + ']';
      })
      .filter(function(d) {
        return d.x < width / 2;
      })
      .attr('x', 6 + sankey.nodeWidth())
      .attr('text-anchor', 'start');
  };

  $scope.$watch('esResponse', function(resp) {
    if (resp) {
      var data = sankeyAggResponse($scope.vis, resp);
      globalData = data;
      _buildVis(data);
    }
  });

});
